from django.apps import AppConfig


class PageAboutConfig(AppConfig):
    name = 'page_about'
