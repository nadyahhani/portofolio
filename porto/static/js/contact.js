//popup
function closeModal(){
  console.log("closing modal");
  $(".modal").css("display","none");
}
function showModal(modalid){
  console.log("showing modal " + modalid);
  $("#" + modalid).css("display","block");
}
// burger
var navisopen = false;
function navbaropen(){
  console.log("nav is opening");

  var nav = document.getElementsByClassName("navbar");
  if (!navisopen){
    nav[0].style.opacity = 1;
    nav[0].style.visibility ="visible";

  }
  else{
    nav[0].style.opacity = 0;
    nav[0].style.visibility ="hidden";
  }
  navisopen = !navisopen;

}
var nightmode_on = false;
// night mode toggle
function togglenight(){
  if (!nightmode_on){
    $("html").css("background-color","#3d4348");
    $(".title").css("color", "white");
    $("h1").css("color", "white");
    $("h2").css("color", "white");
    $("h3").css("color", "white");
    $(".line").css("border-top", "1px solid white");
    $(".circle").css("border", "1px solid white");
    $(".square-bg").css("border", "1px solid white");
    $("p").css("color", "white");
    $("a").css("color", "white");
    $(".navbar-side").css("background-color","#3d4348");
    $(".burger-line").css("border-color", "white");
    $(".container").css("border-color", "white");
    $(".comment-form").css("border-color", "white");
    $(".comment").css("border-color", "white");
    $(".comment").css("background-color", "#3d4348");
    $(".social-media").css("filter", "invert(1)");
    $(".modal-box").css("background-color", "#3d4348");
    $(".modal-box").css("border-color", "white");

    var exq = document.getElementsByClassName("x");
    console.log(exq);
    for (let i = 0; i < exq.length; i++){
      console.log("fox");
      exq[i].classList.add('wx');

    }

  }
  else{
    $("html").css("background-color","white");
    $(".title").css("color", "black");
    $("h1").css("color", "black");
    $("h2").css("color", "black");
    $("h3").css("color", "black");
    $(".line").css("border-top", "1px solid black");
    $(".circle").css("border", "1px solid black");
    $(".square-bg").css("border", "1px solid black");
    $("p").css("color", "black");
    $("a").css("color", "black");
    $(".navbar-side").css("background-color","white");
    $(".burger-line").css("border-color", "black");
    $(".container").css("border-color", "black");
    $(".comment-form").css("border-color", "black");
    $(".comment").css("border-color", "black");
    $(".comment").css("background-color", "white");
    $(".social-media").css("filter", "invert(0)");
    $(".modal-box").css("background-color", "white");
    $(".modal-box").css("border-color", "#3d4348");

    var exw = document.getElementsByClassName("x");
    console.log(exq);
    for (let i = 0; i < exw.length; i++){
      console.log("fox");
      exw[i].classList.remove('wx');

    }
  }
  nightmode_on = !nightmode_on;
}
