//loading screen
var loadingscreen = document.getElementById("loading-screen");
// var image1 = document.getElementsByClassName("foto");
// var image2 = document.getElementsByClassName("foto-right");
//
// var loadedimages = 0;
// for (let i = 0; i < image1.length; i++){
//   image1[i].addEventListener("load", imageloaded(), false);
// }
// for (let i = 0; i < image2.length; i++){
//   image2[i].addEventListener("load", imageloaded(), false);
// }
// function imageloaded(){
//   loadedimages++;
//   let length = image1.length + image2.length;
//   if (loadedimages == length){
//     loadingscreen.style.display = "None";
//   }
// }

var shapes = document.getElementsByClassName('shape');
var squares = document.getElementsByClassName('square-bg');
var xs = document.getElementsByClassName('x');
var nightmode_on = false;

var deg = 1;
$(".parallax").scroll(function () {
  for (let i = 0; i < shapes.length; i++){
    shapes[i].style.transform = "rotate(" + deg + "deg)";
  }
  deg++;
  if (deg > 180){
    deg = 0;
  }
});
// burger
var navisopen = false;
function navbaropen(){
  console.log("nav is opening");

  var nav = document.getElementsByClassName("navbar");
  if (!navisopen){
    nav[0].style.opacity = 1;
    nav[0].style.visibility ="visible";

  }
  else{
    nav[0].style.opacity = 0;
    nav[0].style.visibility ="hidden";
  }
  navisopen = !navisopen;

}

// night mode toggle
function togglenight(){
  if (!nightmode_on){
    $("html").css("background-color","#3d4348");
    $(".title").css("color", "white");
    $("h1").css("color", "white");
    $(".line").css("border-top", "1px solid white");
    $(".circle").css("border", "1px solid white");
    $(".square-bg").css("border", "1px solid white");
    $("p").css("color", "white");
    $("a").css("color", "white");
    $(".navbar-side").css("background-color","#3d4348");
    $(".burger-line").css("border-color", "white");

    var exq = document.getElementsByClassName("x");
    console.log(exq);
    for (let i = 0; i < exq.length; i++){
      console.log("fox");
      exq[i].classList.add('wx');

    }

  }
  else{
    $("html").css("background-color","white");
    $(".title").css("color", "black");
    $("h1").css("color", "black");
    $(".line").css("border-top", "1px solid black");
    $(".circle").css("border", "1px solid black");
    $(".square-bg").css("border", "1px solid black");
    $("p").css("color", "black");
    $("a").css("color", "black");
    $(".navbar-side").css("background-color","white");
    $(".burger-line").css("border-color", "black");



    var exw = document.getElementsByClassName("x");
    console.log(exq);
    for (let i = 0; i < exw.length; i++){
      console.log("fox");
      exw[i].classList.remove('wx');

    }
  }
  nightmode_on = !nightmode_on;
}

// ketikan title

var TxtRotate = function(el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 8) || 1000;
  this.txt = '';
  this.tick();
  this.isDeleting = false;
};

TxtRotate.prototype.tick = function() {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

  var that = this;
  var delta = 300 - Math.random() * 100;

  if (this.isDeleting) { delta /= 2; }

  if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === '') {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
  }

  setTimeout(function() {
    that.tick();
  }, delta);
};

window.onload = function() {
  loadingscreen.style.display = "None";
  var elements = document.getElementsByClassName('txt-rotate');
  for (var i=0; i<elements.length; i++) {
    var toRotate = elements[i].getAttribute('data-rotate');
    var period = elements[i].getAttribute('data-period');
    if (toRotate) {
      new TxtRotate(elements[i], JSON.parse(toRotate), period);
    }
  }
  // INJECT CSS
  var css = document.createElement("style");
  css.type = "text/css";
  css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #666 }";
  document.body.appendChild(css);
};
