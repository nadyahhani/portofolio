from django.apps import AppConfig


class PageLandingConfig(AppConfig):
    name = 'page_landing'
