from django.db import models

# Create your models here.
class Comment(models.Model):
    nama = models.CharField(max_length=27)
    comment = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

