from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Comment


# Create your views here.
response={}
def index(request):
    comments = Comment.objects.all().order_by('-created_date')
    response['comments'] = comments
    return render(request, 'contact.html', response)

def add_comment(request):
    if request.method == "POST":
        nama = request.POST['Name']
        if nama == '':
            nama = "anonymous"
        isi = request.POST['Comment']
        Comment.objects.create(nama=nama, comment=isi)
        # Comment.save()
    return HttpResponseRedirect('/contact/')
